// ignore: unused_import
// ignore_for_file: unused_import, duplicate_ignore

import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
// ignore: undefined_shown_name
import 'package:pizza_app/main.dart' show PizzaApp;

Future<void> main() async {
  testWidgets('Pizza App Test', (WidgetTester tester) async {
    // Build our app and trigger a frame.
    await tester.pumpWidget(PizzaApp() as Widget);

    // Verify that our app starts with an empty cart.
    expect(find.text('Panier vide'), findsOneWidget);

    // Select a pizza.
    await tester.tap(find.text('Margherita'));
    await tester.pump();

    // Verify that the selected pizza appears in the cart.
    expect(find.text('Panier : Margherita'), findsOneWidget);

    // Tap the "Place Order" button.
    await tester.tap(find.text('Passer la commande'));
    await tester.pump();

    // Verify that the order is placed and the cart is cleared.
    expect(find.text('Commande passée'), findsOneWidget);
    expect(find.text('Panier vide'), findsOneWidget);
  });
}

class PizzaApp {
}

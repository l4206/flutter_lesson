import 'package:flutter/material.dart';

class LoginPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Connexion/Inscription'),
      ),
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage("assets/images/bg.png"),
            fit: BoxFit.cover,
          ),
        ),
        padding: EdgeInsets.all(16.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Text(
              'Bienvenue chez DBS Pizza !',
              style: TextStyle(fontSize: 24.0, fontWeight: FontWeight.bold),
              textAlign: TextAlign.center,
            ),
            SizedBox(height: 16.0),
            Container(
              height: 50.0, // Contrôle la hauteur du bouton 'Se connecter'
              child: ElevatedButton(
                onPressed: () {
                  // Naviguer vers la page d'accueil lorsque l'utilisateur appuie sur le bouton de connexion
                  Navigator.pushNamed(context, '/login');
                },
                child: Text('Se connecter'),
              ),
            ),
            SizedBox(height: 8.0),
            Container(
              color: Colors.white,
              height: 50.0, // Contrôle la hauteur du bouton 'S'inscrire'
              child: OutlinedButton(
                style: OutlinedButton.styleFrom(
                  side: BorderSide(color: Colors.blue), // Couleur de la bordure
                ),
                onPressed: () {
                  // Naviguer vers la page d'accueil lorsque l'utilisateur appuie sur le bouton d'inscription
                  Navigator.pushNamed(context, '/signin');
                },
                child: Text('S\'inscrire'),
              ),
            ),
            SizedBox(height: 8.0),
          ],
        ),
      ),
    );
  }
}
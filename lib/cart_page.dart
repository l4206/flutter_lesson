import 'package:flutter/material.dart';

class Pizza {
  String name;
  double price;

  Pizza({required this.name, required this.price});
}

class CartItem {
  Pizza pizza;
  int quantity;

  CartItem({required this.pizza, required this.quantity});
}

class CartPage extends StatefulWidget {
  final List<Map<String, dynamic>> selectedPizzas;
  final Map<String, Map<String, double>> pizzaSizes;

  CartPage({required this.selectedPizzas, required this.pizzaSizes});

  @override
  _CartPageState createState() => _CartPageState();
}

class _CartPageState extends State<CartPage> {
  List<CartItem> cartItems = [];

  @override
  void initState() {
    super.initState();
    populateCartItems();
  }

  @override
  void didUpdateWidget(CartPage oldWidget) {
    super.didUpdateWidget(oldWidget);
    populateCartItems();
  }

  void populateCartItems() {
    cartItems = widget.selectedPizzas.map((selectedPizza) {
      final pizza = selectedPizza['pizza'];
      final size = selectedPizza['size'];
      final count = selectedPizza['count'];
      final price = widget.pizzaSizes[pizza]![size]!;

      final pizzaObject = Pizza(name: pizza, price: price);

      return CartItem(pizza: pizzaObject, quantity: count);
    }).toList();
  }

  void removePizzaFromCart(int index) {
    setState(() {
      cartItems.removeAt(index);
    });
  }

  void increasePizzaCount(int index) {
    setState(() {
      if (cartItems[index].quantity < 10) {
        cartItems[index].quantity++;
      }
    });
  }

  void decreasePizzaCount(int index) {
    setState(() {
      if (cartItems[index].quantity > 1) {
        cartItems[index].quantity--;
      }
    });
  }

  double calculateTotalPrice() {
    double total = 0;
    for (final cartItem in cartItems) {
      total += cartItem.pizza.price * cartItem.quantity;
    }
    return total;
  }

  void _showOrderSummary() {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text('Récapitulatif de la commande'),
              IconButton(
                icon: Icon(Icons.close, size: 20),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ],
          ),
          content: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: [
              for (final cartItem in cartItems)
                ListTile(
                  title: Text('${cartItem.pizza.name}'),
                  subtitle: Text('Quantité: ${cartItem.quantity}'),
                ),
              SizedBox(height: 10),
              Text(
                'Montant total : ${calculateTotalPrice().toStringAsFixed(2)} €',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ],
          ),
          actions: [
            Row(
              children: [
                TextButton(
                  child: Text('Continuer'),
                  onPressed: () {
                    Navigator.of(context).pop();
                    // Naviguer vers la page de paiement ou effectuer d'autres actions
                  },
                ),
                Spacer(),
                TextButton(
                  child: Text('Paiement'),
                  onPressed: () {
                    Navigator.of(context).pop();
                    // Naviguer vers la page de paiement ou effectuer d'autres actions
                  },
                ),
              ],
            ),
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Panier'),
      ),
      body: ListView.builder(
        itemCount: cartItems.length + 1, // Ajout d'un élément pour le montant total
        itemBuilder: (context, index) {
          if (index < cartItems.length) {
            final cartItem = cartItems[index];
            final pizza = cartItem.pizza;
            final name = pizza.name;
            final price = pizza.price;

            return ListTile(
              title: Text('$name - ${price.toStringAsFixed(2)} €'),
              trailing: Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  IconButton(
                    icon: Icon(Icons.remove),
                    onPressed: () {
                      decreasePizzaCount(index);
                    },
                  ),
                  Text(cartItem.quantity.toString()),
                  IconButton(
                    icon: Icon(Icons.add),
                    onPressed: () {
                      increasePizzaCount(index);
                    },
                  ),
                  IconButton(
                    icon: Icon(Icons.delete),
                    onPressed: () {
                      removePizzaFromCart(index);
                    },
                  ),
                ],
              ),
            );
          } else {
            // Affichage du montant total
            return ListTile(
              title: Text(
                'Montant total : ${calculateTotalPrice().toStringAsFixed(2)} €',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            );
          }
        },
      ),
      floatingActionButton: FloatingActionButton.extended(
        onPressed: _showOrderSummary,
        label: Text('Passer la commande'),
        icon: Icon(Icons.shopping_cart),
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:pizza_app/login_page.dart';
import 'package:pizza_app/cart_page.dart';
import 'package:pizza_app/pages/login.dart';
import 'package:pizza_app/pages/signin.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

void main() {
  final pizzaSizes = {
    'Margherita': {
      'Small': 10.00,
      'Medium': 12.00,
      'Large': 14.00,
    },
    'Pepperoni': {
      'Small': 11.00,
      'Medium': 13.00,
      'Large': 15.00,
    },
    'Hawaïenne': {
      'Small': 12.00,
      'Medium': 14.00,
      'Large': 16.00,
    },
    'Végétarienne': {
      'Small': 11.00,
      'Medium': 13.00,
      'Large': 15.00,
    },
  };

  runApp(MyApp(pizzaSizes: pizzaSizes));
}

class MyApp extends StatelessWidget {
  final Map<String, Map<String, double>> pizzaSizes;

  MyApp({required this.pizzaSizes});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'DBS Pizza',
      initialRoute: '/',
      routes: {
        '/': (context) => LoginPage(),
        '/home': (context) => PizzaHomePage(),
        '/cart': (context) => CartPage(
          selectedPizzas: ModalRoute.of(context)!.settings.arguments as List<Map<String, dynamic>>,
          pizzaSizes: pizzaSizes,
        ),
        '/signin': (context) => SignInPage(),
        '/login': (context) => LogInInPage(),
      },
    );
  }
}

class PizzaHomePage extends StatefulWidget {
  @override
  _PizzaHomePageState createState() => _PizzaHomePageState();
}

class _PizzaHomePageState extends State<PizzaHomePage> {
  List<String> pizzas = [
    'Margherita',
    'Pepperoni',
    'Hawaïenne',
    'Végétarienne',
  ];

  List<Map<String, dynamic>> selectedPizzas = [];

  int cartItemCount = 0;

  Map<String, double> pizzaPrices = {
    'Margherita': 10.00,
    'Pepperoni': 11.00,
    'Hawaïenne': 12.00,
    'Végétarienne': 11.00,
  };

  List<List<String>> ingredients = [
    ['Sauce tomate', 'Fromage', 'Basilic'],
    ['Sauce tomate', 'Fromage', 'Pepperoni'],
    ['Sauce tomate', 'Fromage', 'Jambon', 'Ananas'],
    ['Sauce tomate', 'Fromage', 'Poivrons', 'Champignons', 'Oignons'],
  ];

  Map<String, Map<String, double>> pizzaSizes = {
    'Margherita': {
      'Small': 10.00,
      'Medium': 12.00,
      'Large': 14.00,
    },
    'Pepperoni': {
      'Small': 11.00,
      'Medium': 13.00,
      'Large': 15.00,
    },
    'Hawaïenne': {
      'Small': 12.00,
      'Medium': 14.00,
      'Large': 16.00,
    },
    'Végétarienne': {
      'Small': 11.00,
      'Medium': 13.00,
      'Large': 15.00,
    },
  };

  Map<String, String> selectedPizzaSizes = {
    'Margherita': 'Small',
    'Pepperoni': 'Small',
    'Hawaïenne': 'Small',
    'Végétarienne': 'Small',
  };

  bool isPizzaSelected(int index) {
    final pizza = pizzas[index];
    return selectedPizzas.any((item) => item['pizza'] == pizza);
  }

  Future getAllPizzas() async {
    String? token = await storage.read(key: "token");
    var url = Uri.parse('http://beta.letsmakefair.com/items/Pizza');
    var headers = {'Authorization': 'Bearer $token'};
    try {
      var response = await http.get(url, headers: headers);
      // Handle the response here
      if (response.statusCode >= 200 && response.statusCode < 300) {
            if (response.statusCode >= 200 && response.statusCode < 300) {
              var jsonResponse = jsonDecode(response.body);
              return jsonResponse["data"];
            }
        }}catch(e){}
    return [];
  }

  void togglePizzaSelection(int index) {
    setState(() {
      final pizza = pizzas[index];
      final pizzaSize = selectedPizzaSizes[pizza]!;
      final existingIndex = selectedPizzas.indexWhere((item) => item['pizza'] == pizza && item['size'] == pizzaSize);

      if (existingIndex != -1) {
        if (selectedPizzas[existingIndex]['count'] < 10) {
          selectedPizzas[existingIndex]['count']++;
          cartItemCount++;  // déplacez cette ligne ici
        }
      } else {
        selectedPizzas.add({
          'pizza': pizza,
          'size': pizzaSize,
          'count': 1,
        });
        cartItemCount++;
      }
    });
  }

  void showIngredientsDialog(int index) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(pizzas[index]),
          content: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: ingredients[index]
                .map((ingredient) => Text('- $ingredient'))
                .toList(),
          ),
          actions: [
            TextButton(
              child: Text('Fermer'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  void removePizzaFromCart(int index) {
    setState(() {
      selectedPizzas.removeAt(index);
    });
  }

  void increasePizzaCount(int index) {
    setState(() {
      if (selectedPizzas[index]['count'] < 10) {
        selectedPizzas[index]['count']++;
      }
    });
  }

  void decreasePizzaCount(int index) {
    setState(() {
      if (selectedPizzas[index]['count'] > 1) {
        selectedPizzas[index]['count']--;
      }
    });
  }

  double calculateTotalPrice() {
    double total = 0;
    for (final selectedPizza in selectedPizzas) {
      final pizza = selectedPizza['pizza'];
      final size = selectedPizza['size'];
      final count = selectedPizza['count'];
      final price = pizzaSizes[pizza]![size]!;
      total += price * count;
    }
    return total;
  }

  @override
  Widget build(BuildContext context) {
    var gettedPizzas = getAllPizzas();
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.red,
        centerTitle: true,
        title: Text('DBS'),
        actions: [
          Stack(
            children: <Widget>[
              IconButton(
                icon: Icon(Icons.shopping_cart),
                onPressed: () {
                  Navigator.of(context).pushNamed('/cart', arguments: selectedPizzas);
                },
              ),
              if (cartItemCount > 0) ...[
                Positioned(
                  right: 0,
                  top: 0,
                  child: Container(
                    padding: EdgeInsets.all(1),
                    decoration: BoxDecoration(
                      color: Colors.red,
                      borderRadius: BorderRadius.circular(6),
                    ),
                    constraints: BoxConstraints(
                      minWidth: 12,
                      minHeight: 12,
                    ),
                    child: Text(
                      '$cartItemCount',
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 8,
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ),
                ),
              ]
            ],
          ),
        ],
      ),
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage('assets/images/pizza.png'),
            fit: BoxFit.cover,
          ),
        ),
        child: ListView.builder(
        itemCount: pizzas.length,
        itemBuilder: (BuildContext context, int index) {
          if (index < pizzas.length) {
            final pizza = pizzas[index];
            final isSelected = isPizzaSelected(index);
            final pizzaIngredients = ingredients[index];

            return Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                ListTile(
                  title: Row(
                    children: [
                      Text(pizza),
                      Spacer(),
                      DropdownButton<String>(
                        value: selectedPizzaSizes[pizza],
                        onChanged: (String? newSize) {
                          setState(() {
                            selectedPizzaSizes[pizza] = newSize!;
                          });
                        },
                        items: pizzaSizes[pizza]!.keys
                            .map<DropdownMenuItem<String>>(
                              (String size) => DropdownMenuItem<String>(
                                value: size,
                                child: Text(size),
                              ),
                            )
                            .toList(),
                      ),
                      SizedBox(width: 8),
                      Text(
                        'Prix : ${pizzaSizes[pizza]![selectedPizzaSizes[pizza]]!.toStringAsFixed(2)} €',
                        style: TextStyle(fontWeight: FontWeight.bold),
                      ),
                    ],
                  ),
                  trailing: isSelected
                      ? IconButton(
                          icon: Icon(Icons.add),
                          onPressed: () {
                            togglePizzaSelection(index);
                          },
                        )
                      : IconButton(
                          icon: Icon(Icons.add),
                          onPressed: () {
                            togglePizzaSelection(index);
                          },
                        ),
                  onLongPress: () {
                    showIngredientsDialog(index);
                  },
                ),
                SizedBox(height: 8),
                Padding(
                  padding: EdgeInsets.only(left: 16),
                  child: Text(
                    'Ingrédients : ${pizzaIngredients.join(", ")}',
                    style: TextStyle(
                      fontStyle: FontStyle.italic,
                      color: Colors.grey,
                    ),
                  ),
                ),
                SizedBox(height: 16),
              ],
            );
          } else {
            return Column(
              children: [
                SizedBox(height: 20),
                Text(
                  'Panier',
                  style: TextStyle(fontSize: 24, fontWeight: FontWeight.bold),
                ),
                SizedBox(height: 10),
                ListView.builder(
                  shrinkWrap: true,
                  itemCount: selectedPizzas.length,
                  itemBuilder: (BuildContext context, int index) {
                    final selectedPizza = selectedPizzas[index];
                    final pizza = selectedPizza['pizza'];
                    final size = selectedPizza['size'];
                    final count = selectedPizza['count'];

                    return ListTile(
                      title: Text('$pizza - $size'),
                      trailing: Row(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          IconButton(
                            icon: Icon(Icons.remove),
                            onPressed: () {
                              decreasePizzaCount(index);
                            },
                          ),
                          Text(count.toString()),
                          IconButton(
                            icon: Icon(Icons.add),
                            onPressed: () {
                              increasePizzaCount(index);
                            },
                          ),
                          IconButton(
                            icon: Icon(Icons.delete),
                            onPressed: () {
                              removePizzaFromCart(index);
                            },
                          ),
                        ],
                      ),
                      onTap: () {
                        showIngredientsDialog(pizzas.indexOf(pizza));
                      },
                      subtitle: Text('Quantité: $count'),
                    );
                  },
                ),
                SizedBox(height: 10),
                Text(
                  'Montant total : ${calculateTotalPrice().toStringAsFixed(2)} €',
                  style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                ),
                SizedBox(height: 20),
              ],
            );
          }
        },
      ),
    ),);
  }
}

// class Database {
//   static final Database _instance = Database._internal();

//   factory Database() {
//     return _instance;
//   }

//   Database._internal();

//   void saveOrder(Map<String, dynamic> order) {
//     // Code pour sauvegarder la commande dans la base de données
//     print('Commande sauvegardée : $order');
//   }
// }
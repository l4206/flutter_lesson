// import 'cart.dart';

// class Order {
//   List<String> pizzas;
//   double total;

//   Order({required this.pizzas, required this.total});
// }

// List<Order> orders = [];

// void placeOrder(List<String> selectedPizzas, List<double> pizzaPrices) {
//   final order = Order(pizzas: List.from(selectedPizzas), total: calculateTotal(selectedPizzas, pizzaPrices));
//   orders.add(order);
//   selectedPizzas.clear();
// }

// void showPastOrders() {
//   if (orders.isEmpty) {
//     print('No past orders.');
//   } else {
//     print('Past Orders:');
//     for (Order order in orders) {
//       print('Pizzas: ${order.pizzas.join(", ")}');
//       print('Total: ${order.total.toStringAsFixed(2)} €');
//       print('---');
//     }
//   }
// }

// import 'package:flutter/material.dart';

// double calculateTotal(List<String> selectedPizzas, List<double> pizzaPrices) {
//   double totalPrice = 0.0;
//   for (String pizza in selectedPizzas) {
//     int index = selectedPizzas.indexOf(pizza);
//     totalPrice += pizzaPrices[index];
//   }
//   return totalPrice;
// }

// Widget buildCartSection(List<String> selectedPizzas, List<double> pizzaPrices) {
//   return Column(
//     children: [
//       Text(
//         'Panier',
//         style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
//       ),
//       SizedBox(height: 10),
//       ListView.builder(
//         shrinkWrap: true,
//         itemCount: selectedPizzas.length,
//         itemBuilder: (BuildContext context, int index) {
//           final pizza = selectedPizzas[index];
//           return ListTile(
//             title: Text(pizza),
//             trailing: IconButton(
//               icon: Icon(Icons.delete),
//               onPressed: () {
//                 selectedPizzas.removeAt(index);
//               },
//             ),
//           );
//         },
//       ),
//       SizedBox(height: 10),
//       Text(
//         'Total: ${calculateTotal(selectedPizzas, pizzaPrices).toStringAsFixed(2)} €',
//         style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
//       ),
//     ],
//   );
// }

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class SuccessPopup extends StatelessWidget {
  final String message;

  SuccessPopup({required this.message});

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text('User created successfully!'),
      content: Text(message),
      actions: [
        TextButton(
          child: Text('OK'),
          onPressed: () {
            Navigator.of(context).pushNamed('/');
          },
        ),
      ],
    );
  }
}

Future<void> registerUser(email,password,context) async {
  var url = Uri.parse('http://beta.letsmakefair.com/users');
  var headers = {'Content-Type': 'application/json'};
  var body = '{"email":"' + email + '", "password": "'+password+'"}';

  try {
    var response = await http.post(url, headers: headers, body: body);

    // Handle the response here
    if (response.statusCode >= 200 && response.statusCode < 300) {
      print('User registration successful!');
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return SuccessPopup(message: 'User created successfully!');
        },
      );
    } else {
      print('User registration failed with status code: ${response.statusCode}');
    }
  } catch (e) {
    print('Error occurred while registering user: $e');
  }
}

class SignInPage extends StatefulWidget {
  @override
  _SignInPageState createState() => _SignInPageState();
}

class _SignInPageState extends State<SignInPage> {
  final _formKey = GlobalKey<FormState>();
  String _email = "";
  String _password = "";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Sign In'),
      ),
      body: Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage('assets/images/form.png'),
              fit: BoxFit.cover,
            ),
          ),
        padding: const EdgeInsets.all(32.0),
        child: Form(
          key: _formKey,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              TextFormField(
                decoration: InputDecoration(
                  labelText: 'Email',
                ),
                validator: (value) {
                  if (value?.isEmpty ?? true) {
                    return 'Please enter your email';
                  }
                  return null;
                },
                onSaved: (value) {
                  _email = value!;
                },
              ),
              TextFormField(
                decoration: InputDecoration(
                  labelText: 'Password',
                ),
                obscureText: true,
                validator: (value) {
                  if (value?.isEmpty ?? true) {
                    return 'Please enter your password';
                  }
                  return null;
                },
                onSaved: (value) {
                  _password = value!;
                },
              ),
              SizedBox(height: 16.0),
                ElevatedButton(
                child: Text('Sign In'),
                onPressed: () {
                  if (_formKey.currentState?.validate() ?? false) {
                    _formKey.currentState?.save();
                    registerUser(_email, _password, context);
                  }
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}
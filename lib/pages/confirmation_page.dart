import 'package:flutter/material.dart';

class ConfirmationPage extends StatelessWidget {
  final List<String> pizzas;
  final double total;

  ConfirmationPage({required this.pizzas, required this.total});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Order Confirmation'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              'Order Details:',
              style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
            ),
            SizedBox(height: 20),
            Text('Pizzas: ${pizzas.join(", ")}'),
            SizedBox(height: 10),
            Text('Total: $total €', style: TextStyle(fontWeight: FontWeight.bold)),
          ],
        ),
      ),
    );
  }
}

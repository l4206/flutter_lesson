import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'dart:convert';

// Create storage
final storage = new FlutterSecureStorage();

class SuccessPopup extends StatelessWidget {
  final String message;

  SuccessPopup({required this.message});

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text('password or email incorrect!'),
      content: Text(message),
      actions: [
        TextButton(
          child: Text('OK'),
          onPressed: () {},
        ),
      ],
    );
  }
}

Future<void> registerUser(email, password, context) async {
  var url = Uri.parse('http://beta.letsmakefair.com/auth/login');
  var headers = {'Content-Type': 'application/json'};
  var body = '{"email":"' + email + '", "password": "' + password + '"}';

  try {
    var response = await http.post(url, headers: headers, body: body);
    // Handle the response here
    if (response.statusCode >= 200 && response.statusCode < 300) {
    try {
      var response = await http.post(url, headers: headers, body: body);
      if (response.statusCode >= 200 && response.statusCode < 300) {
        var jsonResponse = jsonDecode(response.body);
        var token = jsonResponse['data']["access_token"];
        await storage.write(key: "token", value: token);
      }
    } catch (e) {
      print('Error occurred while registering user: $e');
    }
      Navigator.of(context).pushNamed('/home');
    } else {
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return SuccessPopup(message: 'Email or password incorrect!');
        },
      );
    }
  } catch (e) {
    print('Error occurred while registering user: $e');
  }
}

class LogInInPage extends StatefulWidget {
  @override
  _LogInInPageState createState() => _LogInInPageState();
}

class _LogInInPageState extends State<LogInInPage> {
  final _formKey = GlobalKey<FormState>();
  String _email = "";
  String _password = "";

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Scaffold(
        appBar: AppBar(
          title: Text('Sign In'),
        ),
        body: Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage('assets/images/form.png'),
              fit: BoxFit.cover,
            ),
          ),
          child: Padding(
            padding: const EdgeInsets.all(32.0),
            child: Form(
              key: _formKey,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  TextFormField(
                    decoration: InputDecoration(
                    labelText: 'Email',
                  ),
                  validator: (value) {
                    if (value?.isEmpty ?? true) {
                      return 'Please enter your email';
                    }
                    return null;
                  },
                  onSaved: (value) {
                    _email = value!;
                  },
                ),
                TextFormField(
                  decoration: InputDecoration(
                    labelText: 'Password',
                  ),
                  obscureText: true,
                  validator: (value) {
                    if (value?.isEmpty ?? true) {
                      return 'Please enter your password';
                    }
                    return null;
                  },
                  onSaved: (value) {
                    _password = value!;
                  },
                ),
                SizedBox(height: 16.0),
                ElevatedButton(
                  child: Text('Sign In'),
                  onPressed: () {
                    if (_formKey.currentState?.validate() ?? false) {
                      _formKey.currentState?.save();
                      registerUser(_email, _password, context);
                    }
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    ),);
  }
}
# DBS Pizza
Une application de vente de pizzas en ligne.

## Description
L'application de vente de pizzas en ligne permet aux utilisateurs de créer un compte, se connecter, parcourir un menu de pizzas, passer des commandes. Les utilisateurs peuvent également choisir la taille des pizzas tout en visualisant le nombre d’élément dans le panier depuis l’écran de sélection des pizzas.

## Demo

![](demo.gif)

## Fonctionnalités
* Affichage du menu des pizzas disponibles.
* Ajout/Retrait de pizzas au panier.
* Visualisation du panier.
* Visualisation du nombre d’élément dans le panier depuis la vue du menu des pizzas.
* Visualisation du prix.
* Gestion des utilisateurs (inscription, connexion).

## Installation
1. Cloner le dépôt GitHub :
git clone https://github.com/votre-utilisateur/votre-projet.git

2. Installer les dépendances :
Dans le Terminal VsCode avec Flutter/Dart :  flutter pub get

3. Exécuter l'application :
flutter run 

## Technologies utilisées
* Visual Studio Code
* Flutter
* Dart
* Directus

## Auteurs
* Sovre Lucas
* Dominique Edouard
* Bonneville Leonard

## Remarques supplémentaires
* L'application de vente de pizzas en ligne est actuellement en cours de développement et certaines fonctionnalités peuvent ne pas être entièrement implémentées : on a réussi à implémenter la base de données mais les liens avec celles-ci ne sont pas fonctionnels, on utilise donc une base de données brut.
* Les données présentées dans l'application sont des données fictives utilisées uniquement à des fins de démonstration.
